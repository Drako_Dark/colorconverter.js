# Color Converter README #

Bien aquí les comparto este simple script en JavaScript para todo el que lo quiera usar en sus proyectos.

Su funcionalidad es muy simple, hacer conversiones de color de RGB a HEX y viceversa, yo lo uso mucho en mis proyectos donde trabajo con JavaScript y colores.

Pronto pondré más información de cómo usarlo y lo iré actualizando pero igual tú le puedes agregar tus propias funciones dependiendo de tus necesidades.

**NOTA***
El index.html asociado usa jQuery y jQuery UI y fueron agregados a este repositorio para mostrar las funcionalidades de Color Converter, tales extensiones (jQuery y jQuery UI) son de sus respectivos dueños y deberán buscar más información en sus respectivas web.

**Drako Dark** – *[Drako Dark Company 2015](http://www.drakodark.com/)*