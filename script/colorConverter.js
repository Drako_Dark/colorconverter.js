function color(R,G,B) {
	this.version = '1.0';
	this.autor = 'Drako Dark';
	this.url = '';
	
	if(R.toString().substr(0, 1) == '#') {
		var color = new HEXtoRGB(R);
		
		this.hex = R;
		this.r = color.r;
		this.g = color.g;
		this.b = color.b;
		this.rgb = color.r + ',' + color.g + ',' + color.b;
	}
	else {
		var color = new RGBtoHEX(R,G,B);
		
		this.hex = color.hex;
		this.r = R;
		this.g = G;
		this.b = B;
		this.rgb = R + ',' + G + ',' + B;
	}
	
	//funciones
	function RGBtoHEX(R,G,B) {
		var hex = '#' + ((1 << 24) + (parseInt(R) << 16) + (parseInt(G) << 8) + parseInt(B)).toString(16).slice(1);
		this.hex = hex;
	}
		
	function HEXtoRGB(H) {
		var colorE1 = H;
		var eliminar = '#';
		var colorE2 = colorE1.replace(eliminar, '');
		var hex = parseInt(colorE2, 16);
		this.r = (hex >> 16) & 255;
		this.g = (hex >> 8) & 255;
		this.b = hex & 255;
	}
}
