$(document).on('ready',function(){	
	$('#sliderROJO, #sliderVERDE, #sliderAZUL').slider({
		max: 255,
		min: 0,
		step: 1,
		orientation: 'vertical'
	});
	
	$('#sliderROJO').css({
		'background': '-moz-linear-gradient(top, rgba(255,0,0,1) 0%, rgba(0,0,0,1) 100%)',
		'margin-left': 'auto',
		'margin-right': 'auto'
	});
	$('#sliderVERDE').css({
		'background': '-moz-linear-gradient(top, rgba(0,255,0,1) 0%, rgba(0,0,0,1) 100%)',
		'margin-left': 'auto',
		'margin-right': 'auto'
	});
	$('#sliderAZUL').css({
		'background': '-moz-linear-gradient(top, rgba(0,0,255,1) 0%, rgba(0,0,0,1) 100%)',
		'margin-left': 'auto',
		'margin-right': 'auto'
	});
	
	var defcolor = new color('#0a274f');
	$('#colorHEX').text(defcolor.hex);
	$('#colorRGB').text(defcolor.rgb);
	$('#sliderROJO').slider('value',defcolor.r)
	$('#sliderVERDE').slider('value',defcolor.g)
	$('#sliderAZUL').slider('value',defcolor.b)
	$('#color_view').css('background-color',defcolor.hex);
	
	$('#sliderROJO,#sliderVERDE,#sliderAZUL').slider({
		slide: function(){
			var track1 = $('#sliderROJO').slider('value');
			var track2 = $('#sliderVERDE').slider('value');
			var track3 = $('#sliderAZUL').slider('value');
			colorgen(track1,track2,track3);
		}
	});
	
	function colorgen(R,G,B) {
		var color1 = new color(R,G,B);
		var backgroundColor = {
			'background-color': color1.hex,
			'background-image': 'url(img/blueprint.png), url(img/header-background.png), url(img/mdn-header-gradient.png)',
			'background-repeat': 'repeat, repeat, repeat-x'
		};
		
		$('#color_view').css('background-color',color1.hex);
		document.getElementById('colorHEX').innerHTML = color1.hex;
		document.getElementById('colorRGB').innerHTML = color1.rgb;
		$('body').css(backgroundColor);
	}
});